<?php
/**
 * Created by PhpStorm.
 * User: auris
 * Date: 19/03/17
 * Time: 18:18
 */

namespace Application\Form;

use \Zend\Form\Form;


class BirthdayForm extends Form
{
    public function __construct($name, $options)
    {
        parent::__construct($name, $options);

        $this->addElements();
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'birthday',
            'attributes' => [
                'id' => 'birthday',
                'class' => 'form-control',
            ],
            'options' => [
                'label' => 'Your Birth Date',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'class' => 'form-control',
            ],
            'options' => [
                'label' => 'Your Name',
            ],
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'id' => 'calculate-age',
                'class' => 'btn btn-default',
                'value' => 'Submit',
            ],
        ]);
    }
}