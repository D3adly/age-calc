<?php
/**
 * Created by PhpStorm.
 * User: auris
 * Date: 27/03/17
 * Time: 19:03
 */

namespace Application\Model;


use AgeCalculator\Formatter\AgeFormatter;

/**
 * Class Calculator
 * @package Application\Model
 */
class Calculator
{
    /**
     * @param $data
     * @return bool|string
     */
    public static function calculate($data)
    {
        $ageCalculator = new \AgeCalculator\Calculator();
        $age = $ageCalculator->getAge($data['birthday'], 'm/d/Y G:i A',AgeFormatter::FORMAT_DIVIDED_YDH);

        return $age;
    }

    /**
     * @param $data
     */
    protected static function storeRequest($data)
    {
        FileStorage::write(self::preFormat($data));
    }

    /**
     * @param $data
     * @return array
     */
    protected static function preFormat($data)
    {
        return array(
            $data['name'],
            $data['birthdate'],
        );
    }
}