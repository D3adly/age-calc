<?php

namespace Application\Model;

/**
 * Created by PhpStorm.
 * User: auris
 * Date: 27/03/17
 * Time: 19:24
 */

/**
 * Class Hydrator
 * @package Application\Model
 */
class Hydrator
{
    /**
     * @var
     */
    protected $entity = \Application\Entity\Submission::class;

    /**
     * @param $data
     * @return \Application\Entity\Submission
     */
    public function getHydratedRequest($data)
    {
        /** @var \Application\Entity\Submission $entity */
        $entity = new $this->entity;

        $entity->setBirthDate($data[0]);
        $entity->setName($data[1]);
        $entity->setAge(\Application\Model\Calculator::calculate(array(
            'birthday' => $data[0],
        )));

        return $entity;
    }
}