<?php
/**
 * Created by PhpStorm.
 * User: auris
 * Date: 27/03/17
 * Time: 19:08
 */

namespace Application\Model;


/**
 * Class FileStorage
 * @package Application\Model
 */
class FileStorage
{
    /**
     *
     */
    const FILE = '/data/storage/file/submissions.csv';

    /**
     * @param $data
     */
    public static function write($data)
    {
        $handle = self::getHandle('a+');
        fputcsv($handle, $data);
        fclose($handle);
    }

    /**
     * @return array
     */
    public static function read()
    {
        $handle = self::getHandle();
        $entrySet = array();
        $hydrator = new \Application\Model\Hydrator();
        while($row = fgetcsv($handle)) {
            $entrySet[] = $hydrator->getHydratedRequest($row);
        }
        fclose($handle);
        return $entrySet;
    }

    /**
     *
     */
    public static function clear()
    {
        $handle = self::getHandle('a+');
        fputcsv($handle, array());
        fclose($handle);
    }

    /**
     * @param string $mode
     * @return resource
     */
    public static function getHandle($mode = 'r')
    {
        $file = getcwd().self::FILE;
        if(!file_exists($file)){
            touch($file);
            chmod($file, '0777'); //Should not be open, but for the sake not reconfiguring default zf3 vagrant box leaving as 777.
        }

        return fopen($file, $mode);
    }

}