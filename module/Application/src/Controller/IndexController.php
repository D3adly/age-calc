<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Form\BirthdayForm;
use Application\Model\Calculator;
use Application\Model\FileStorage;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractActionController
{
    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $form = new BirthdayForm('Birth Date', []);
        $age = null;

        if($this->getRequest()->isPost()){
            $form->setData($this->params()->fromPost());
            if($form->isValid()){
                $age = Calculator::calculate($form->getData());
                FileStorage::write($form->getData());
            }
        }
        $submissions = FileStorage::read();

        /** View Output */
        $viewModel = new ViewModel();
        $viewModel->setVariable('form', $form);
        $viewModel->setVariable('age', $age);
        $viewModel->setVariable('submissions', $submissions);
        return $viewModel;
    }
}
