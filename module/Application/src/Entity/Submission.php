<?php
/**
 * Created by PhpStorm.
 * User: auris
 * Date: 27/03/17
 * Time: 19:26
 */

namespace Application\Entity;


/**
 * Class Submission
 * @package Application\Entity
 */
class Submission
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $birthDate;

    /**
     * @var string
     */
    protected $age;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBirthDate(): string
    {
        return $this->birthDate;
    }

    /**
     * @param string $birthDate
     */
    public function setBirthDate(string $birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getAge(): string
    {
        return $this->age;
    }

    /**
     * @param string $age
     */
    public function setAge(string $age)
    {
        $this->age = $age;
    }

}